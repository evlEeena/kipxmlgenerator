package main;

import facade.Facade;

import java.util.*;

/**
 * Created by e.paramonova on 21.05.14.
 */
public class Main {

    private Scanner scanner  = new Scanner(System.in);

    public static void main(String[] args) {
        Main main = new Main();
        main.printUsage();
        while(true) {
            main.commandHandle();
        }
    }

    private void printUsage() {
        System.out.println("Usage: \n" +
            "continue\t Enter\n" +
            "help\t -h\n" +
            "quit\t -q\n\n");
    }

    private void commandHandle() {
        String command = scanner.nextLine();
        if(command.equals("-h")) printUsage();
        else if(command.equals("-q")) {
            System.out.println("Exit..");
            System.exit(1);
        }
        else if(command.equals("")){
            generateDocuments();
        }
        else {
            System.out.println("Not correct command!\n");
            printUsage();
        }
    }
//как-то кривоватенько
    private void generateDocuments(){
        System.out.println("Enter document type(1-15)for genering: ");
        String typeDoc = scanner.nextLine();
        System.out.println("Enter number document for genering: ");
        String numDoc = scanner.nextLine();
        try {
            checkInput(typeDoc, numDoc);
        } catch (InputMismatchException e) {
            System.out.println("You must enter number!!!");
            generateDocuments();
        }

        for(int i = 0; i< Integer.parseInt(numDoc); i++)
            new Facade(Integer.parseInt(typeDoc)).generateDocument();
    }

    private void checkInput(String typeDoc, String numDoc) throws InputMismatchException{
        int typeDocInt, numDocInt;
        try {
            typeDocInt = Integer.parseInt(typeDoc);
            numDocInt = Integer.parseInt(numDoc);
        } catch(NumberFormatException e) {
            throw new InputMismatchException();
        }
        if(typeDocInt < 1 || typeDocInt > 15) throw new InputMismatchException();
        if(numDocInt < 1) throw new InputMismatchException();
    }

}
