package facade;

import facade.archivator.Archivator;
import facade.creator.Creator;
import facade.generator.Generator;

import java.io.File;
import java.util.*;

/**
 * Created by e.paramonova on 21.05.14.
 */
public class Facade {

    private int docType;

    private String tempPath;
    private String xmlPath;
    private String zipPath;

    public Facade(int docType) {
        this.docType = docType;
        initPathes();
    }

    private void initPathes() {
        tempPath = System.getProperty("user.dir") + "\\Templates\\" + docType + "\\";
        xmlPath = System.getProperty("user.dir") + "\\XMLs\\" + docType + "\\";
        zipPath = System.getProperty("user.dir") + "\\ZIPs\\" + docType + "\\";
    }

    public void generateDocument() {
        String[] codes = {"00000", "00001"};
        List<File> filesList = new ArrayList<File>();

        Generator g = new Generator(docType);
        Creator c = new Creator();
        Archivator a = new Archivator();

        g.generateParameter();

        for(String code  : codes) {
            String xmlName = g.generateXMLName(code);
            File xmlFile = (c.createFile(xmlPath + xmlName));
            filesList.add(xmlFile);
            c.copyFile(tempPath + code + ".XML");
            g.editFileNodes(xmlFile);
        }
        a.addFilesToZip(filesList, zipPath + g.generateZIPName());

    }
}
