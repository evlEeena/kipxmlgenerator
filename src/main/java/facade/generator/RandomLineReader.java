package facade.generator;

import javafx.util.Pair;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.*;

/**
 * Created by e.paramonova on 21.05.14.
 */
public class RandomLineReader {
    private List<String> innList;

    public RandomLineReader() {
        try {
            Path path = Paths.get(System.getProperty("user.dir") + "\\Org_and_INN.txt");
            innList = Files.readAllLines(path, Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Pair<String, String> getRandomINNAndOrganization() {
        String[] innOrgArray = (innList.get((new Random()).nextInt(innList.size() - 1))).split(",");
        return  new Pair<String, String>(innOrgArray[0], innOrgArray[1]);
    }
}
