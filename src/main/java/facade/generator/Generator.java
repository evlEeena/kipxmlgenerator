package facade.generator;

import javafx.util.Pair;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by e.paramonova on 21.05.14.
 */
public class Generator {

    private RandomLineReader randomLineReader = new RandomLineReader();

    private String[] docArr = {"V_MSK_DEATH", "V_SPN_ZAJAV", "V_SPN_ANLIS", "V_SPN_R-SUD",
            "V_MSK_OTKAZ", "V_SPN_DOG_NVS", "", "", "V_UVED_END_OPS", "",
            "V_UVED_ZA4ISL", "", "", "", "V_ZAPROS"};

    private Map<String, String> nodeMap = new HashMap<>();
    private String docName;
    private int docCode;

    public Generator(int docCode) {
        this.docCode = docCode;
    }

    public void generateParameter() {
        String curDate = (new SimpleDateFormat("dd.MM.YYYY")).format(new Date());
        nodeMap.put("ДатаДокумента", curDate);

        Pair<String, String> innAndOrgPair = randomLineReader.getRandomINNAndOrganization();
        nodeMap.put("ИНН", innAndOrgPair.getValue());
        nodeMap.put("НаименованиеОрганизации", innAndOrgPair.getKey());

        //generate doc number
        Random rand = new Random();
        String docNum = "";
        for (int i = 0; i < 5; i++) {
            docNum += rand.nextInt(9);
        }
        nodeMap.put("НомерДокументаОрганизации", docNum);

        String nameBundle = "PFR-700-Y-2014" + "-INN-" + nodeMap.get("ИНН") + "-DCK-" + nodeMap.get("НомерДокументаОрганизации")
                + "-" + "00001" + "-" + docArr[docCode - 1] + ".XML";
        nodeMap.put("ИмяПачки", nameBundle);

    }

    public String generateXMLName(String code) {
        docName = "PFR-700-Y-2014" + "-INN-" + nodeMap.get("ИНН") + "-DCK-" + nodeMap.get("НомерДокументаОрганизации")
                + "-" + code + "-" + docArr[docCode - 1] + ".XML";
        return docName;
    }

    public String generateZIPName() {
        String docCodeNum = "";
        if(docCode < 10) docCodeNum = "00" + docCode;
        else docCodeNum = "0" + docCode;

        return nodeMap.get("ИНН") + "-" + docArr[docCode - 1] + "-" + docCodeNum + "-" + nodeMap.get("ДатаДокумента") + ".ZIP";

    }

    public void editFileNodes(File xml) {
        DocumentBuilderFactory dbFactory;
        Document docXML;
        try {
            dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
            docXML = docBuilder.parse(xml);
            docXML.getDocumentElement().normalize();
            for (Map.Entry<String, String> node : nodeMap.entrySet()) {
                editXMLNode(docXML, node.getKey(), node.getValue());
            }
            editXMLNode(docXML, "ИмяФайла", docName);
            Transformer transformer;
            try {
                transformer = TransformerFactory.newInstance().newTransformer();
                DOMSource domSrc = new DOMSource(docXML);
                StreamResult resXML = new StreamResult(xml);
                transformer.setOutputProperty("encoding", "windows-1251");
                transformer.transform(domSrc, resXML);

            } catch (TransformerException e) {
                e.printStackTrace();
            }

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    public void editXMLNode(Document docXML, String nodeName, String newValue) {
        NodeList nodeList = docXML.getElementsByTagName(nodeName);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node fileNameNode = nodeList.item(i);
            fileNameNode.setTextContent(newValue);
        }
    }
}
