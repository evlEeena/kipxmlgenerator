package facade.creator;

import java.io.*;
import java.nio.channels.FileChannel;

/**
 * Created by e.paramonova on 21.05.14.
 */
public class Creator {
    File newFile;
    public Creator() {
    }

    public File createFile(String filePath) {
        newFile =  new File(filePath);
        return newFile;
    }

    public void copyFile(String tempPath) {
        File srcXML = new File(tempPath);
        try {
            FileChannel srcChannel = new FileInputStream(srcXML).getChannel();
            FileChannel destChannel = new FileOutputStream(newFile).getChannel();
            srcChannel.transferTo(0, srcChannel.size(), destChannel);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
