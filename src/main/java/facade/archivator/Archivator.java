package facade.archivator;

import java.io.*;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by e.paramonova on 21.05.14.
 */
public class Archivator {

    static final int BUFFER = 1024;

    public Archivator() {
    }

    public void addFilesToZip(List<File> filesList, String zipPath) {
        byte[] buffer = new byte[1024];

        ZipOutputStream zipOutputStream = null;

        try {
            zipOutputStream = new ZipOutputStream(new FileOutputStream(zipPath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            for(File file : filesList) {
                ZipEntry zipEntry = new ZipEntry(file.getName());
                zipOutputStream.putNextEntry(zipEntry);
                FileInputStream fileInputStream = new FileInputStream(file.getAbsolutePath());
                int len;
                while((len = fileInputStream.read(buffer)) > 0) {
                    zipOutputStream.write(buffer, 0, len);
                }
                fileInputStream.close();
            }
            zipOutputStream.close();
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
